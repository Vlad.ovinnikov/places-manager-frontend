export const environment = {
  production: true,
  API_URL: 'https://places-manager-backend.herokuapp.com/api'
};
