import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { APP_BASE_HREF } from '@angular/common';
import { Observable } from 'rxjs/Rx';

import { RoutesModule } from './../../app.routes';

import { ListComponent, DetailsComponent } from './../';
import { PlaceService } from './../../services';
import { SpinnerComponent } from './../../components';
import { Place } from './../../models';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let service: PlaceService;
  let place: Place;
  let activatedRoute = {
    snapshot: {
      params: { id: '1k23jh23j2h1k3' }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, RoutesModule, InfiniteScrollModule],
      providers: [PlaceService,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: APP_BASE_HREF, useValue: '/places' },
      ],
      declarations: [ ListComponent, DetailsComponent, SpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    place = new Place();
    place.name = 'Hello';

    service = TestBed.get(PlaceService);
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('placeService.get should return place', () => {
    expect(component.placeId).toBe('1k23jh23j2h1k3');
    expect(component.isRequesting).toBeTruthy();
    
    spyOn(service, 'get').and.returnValue(Observable.of(place));

    component.ngOnInit();

    expect(component.isRequesting).toBeFalsy();
    expect(component.place).toBe(place);
    expect(service.get).toHaveBeenCalledWith('1k23jh23j2h1k3');
  });

  it('placeService.get should return error', () => {
    expect(component.placeId).toBe('1k23jh23j2h1k3');
    expect(component.isRequesting).toBeTruthy();

    spyOn(service, 'get').and.returnValue(Observable.throw('error'));

    component.ngOnInit();

    expect(component.isRequesting).toBeFalsy();
    expect(component.place.name).toBe((new Place()).name);
    expect(service.get).toHaveBeenCalledWith('1k23jh23j2h1k3');
    expect(component.errorMessage).toBe('error');
  });

});
