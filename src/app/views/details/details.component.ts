import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { Place } from './../../models';
import { PlaceService } from './../../services';
import { GOOGLE_API } from './../../app.settings';

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  place: Place;
  errorMessage: any;
  isRequesting: boolean;
  private _placeId: string;
  private googleAPI: string;

  public get placeId() { return this._placeId; }

  constructor(private placeService: PlaceService, private activatedRoute: ActivatedRoute) {
    this.place = new Place();
    this.googleAPI = GOOGLE_API;
    this.isRequesting = true;
  }

  ngOnInit() {
    this._placeId = this.activatedRoute.snapshot.params['id'];
    // get place with its id
    this.placeService.get(this._placeId)
      .subscribe(
        (res: Place) => {
          this.isRequesting = false;
          this.place = res;
        },
        (err: any) => {
          this.isRequesting = false;
          this.errorMessage = err;
        }
      );
  }
}
