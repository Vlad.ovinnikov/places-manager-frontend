import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Place } from './../../../models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  placeForm: FormGroup;
  name: FormControl;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.createControls();
    this.createForm();
  }

  private createControls() {
    this.name = new FormControl(null);
  }

  private createForm() {
    this.placeForm = new FormGroup({
      name: this.name
    });
  }

  // do redirect to places url and add query param for searching places
  getPlacesAPI() {
    if(this.name.value) {
      this.router.navigate(['places'], { queryParams: { name: this.placeForm.value.name}});
    }
  }
}
