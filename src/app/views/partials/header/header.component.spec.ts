import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Directive } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { APP_BASE_HREF } from '@angular/common';
import { RouterTestingModule } from "@angular/router/testing";

import { RoutesModule } from './../../../app.routes';

import { HeaderComponent } from './../';
import { ListComponent, DetailsComponent } from './../../';
import { SpinnerComponent } from './../../../components';

@Directive({
    selector: '[routerLink], [routerLinkActive]'
})
class DummyRouterLinkDirective {}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule, RoutesModule, ReactiveFormsModule, FormsModule, InfiniteScrollModule ],
      declarations: [ HeaderComponent, ListComponent, DetailsComponent, SpinnerComponent, DummyRouterLinkDirective ],
      providers: [  { provide: APP_BASE_HREF, useValue : '/places' } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('form name value should be null', () => {
    let name = component.placeForm.controls['name'];
    expect(name.value).toBe(null);
  });

  it('should submit form with not null name', () => {

    let name = component.placeForm.controls['name'];
    expect(name.value).toBe(null);

    name.setValue('cafe');
    expect(name.value).toBe('cafe');

    component.getPlacesAPI();
  });

  it('should submit form with null name', () => {
    let name = component.placeForm.controls['name'];
    expect(name.value).toBe(null);

    component.getPlacesAPI();
  });

});
