import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { Place } from './../../models';
import { PlaceService } from './../../services';
import { GOOGLE_API } from './../../app.settings';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('list', [
      state('fadeIn', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          opacity: 0
        }),
        animate(1000)
      ])
    ])
  ]
})
export class ListComponent implements OnInit, OnDestroy {

  places: Place[];
  errorMessage: any;
  isRequesting: boolean;
  private _perPage: number;
  private _page: number;
  private _allItems: number;
  private _name: string;
  private googleAPI: string;
  private queryParamsEventSub: any;
  private _isScrolled: boolean;

  public get name(){ return this._name; }
  public set name(name: string) { this._name = name; }

  public get allItems(){ return this._allItems; }
  public set allItems(allItems: number) { this._allItems = allItems; }

  public get page(){ return this._page; }
  public set page(page: number) { this._page = page; }

  public get perPage(){ return this._perPage; }
  public set perPage(perPage: number) { this._perPage = perPage; }

  public get isScrolled(){ return this._isScrolled; }
  public set isScrolled(isScrolled: boolean) { this._isScrolled = isScrolled; }

  constructor(private placeService: PlaceService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.googleAPI = GOOGLE_API;
    this.perPage = 8;
    this.page = 1;
    this.places = [];
    this.isScrolled = false;
  }

  ngOnInit() {
    // subscribe for changing query param on searching
    this.queryParamsEventSub = this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(e => {
        this.perPage = 8;
        this.page = 1;
        this.places = [];
        this.getPlaces(this.page);
    });
  }

  /**
   * get places with param
   * @return {Place[]}      list of places
   */
  getPlaces(page: number) {
    this._name = this.activatedRoute.snapshot.queryParams['name'];

    if (this._name) {
      // spinner appears only on page load
      // when scroll it does not appear
      if(!this.isScrolled) {
        this.isRequesting = true;
      }

      this.placeService.getPlacesAPI(this._name, page, this.perPage)
        .subscribe(
          (res: any) => {
            this.allItems = res.allItems;

            for(let key in res.places) {
              this.places.push(res.places[key]);
            }
            this.isScrolled = false;
            this.isRequesting = false;
          },
          (err: any) => {
            this.isRequesting = false;
            this.errorMessage = err;
          }
        );
    }
  }

  onScroll() {
    // get places on scroll event
    if (this.places.length < this.allItems) {
      this.isScrolled = true;
      this.page = this.page + 1;
      this.getPlaces(this.page);
    }
  }


  ngOnDestroy() {
    this.queryParamsEventSub.unsubscribe();
  }

}
