import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { Observable } from 'rxjs/Rx';

import { RoutesModule } from './../../app.routes';

import { ListComponent, DetailsComponent } from './../';
import { PlaceService } from './../../services';
import { SpinnerComponent } from './../../components';
import { Place } from './../../models';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let service: PlaceService;
  // let router: Router;
  let router = {
    navigate: jasmine.createSpy('navigate'),
    events: {
      filter: function(){return {subscribe: function(){}}},
    }
  };

  let activatedRoute = {
    snapshot: {
      queryParams: { name: 'cafe' }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule, InfiniteScrollModule, RouterModule ],
      declarations: [ ListComponent, DetailsComponent, SpinnerComponent ],
      providers: [ PlaceService,
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: Router, useValue: router } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    service = TestBed.get(PlaceService);
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('getPlaces should return places', () => {
    expect(component.name).not.toBeDefined();
    expect(component.places.length).toBe(0);

    spyOn(service, 'getPlacesAPI').and.returnValue(Observable.of({ places: ['values'], allItems: 2 }));

    component.getPlaces(1);
    expect(component.isScrolled).toBeFalsy();
    expect(component.isRequesting).toBeFalsy();
    expect(component.name).toBe('cafe');
    expect(component.allItems).toBe(2);
    expect(component.page).toBe(1);
    expect(component.perPage).toBe(8);
    expect(component.places.length).toBe(1);
    expect(service.getPlacesAPI).toHaveBeenCalled();
  });

  it('getPlaces should return error', () => {
    // not finished
    expect(component.name).not.toBeDefined();
    expect(component.places.length).toBe(0);

    spyOn(service, 'getPlacesAPI').and.returnValue(Observable.throw('error'));

    component.getPlaces(1);

    expect(component.isRequesting).toBeFalsy();
    expect(component.errorMessage).toBe('error');
  });

  it('should scroll to get next items', () => {
    let item1 = new Place();
    let item2 = new Place();
    let item3 = new Place();
    component.places = [item1, item2];
    component.allItems = 4;

    component.onScroll();

    expect(component.isScrolled).toBeTruthy();
    expect(component.page).toBe(2);
    expect(component.allItems).toBe(4);
  });

});
