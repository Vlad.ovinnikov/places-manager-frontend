import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { API_URL, LOCATION, RADIUS, TYPES } from './../../app.settings';
import { Place } from './../../models';

@Injectable()
export class PlaceService {

  placeSelectEvent: EventEmitter<Place>;
  errorMessage: EventEmitter<any>;

  constructor(private http: Http) {
    this.placeSelectEvent = new EventEmitter<Place>();
  }

  getPlacesAPI(name: string, page: number, perPage: number): Observable<Place[]> {
    const params = new URLSearchParams();
    params.set('location', LOCATION);
    params.set('radius', RADIUS);
    params.set('types', TYPES);
    params.set('name', name);
    params.set('page', page.toString());
    params.set('perPage', perPage.toString());

    return this.http.get(`${ API_URL }/places/api`, { params })
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err))
      .finally(() => {
        console.info(`Place.getPlacesAPI: ${ new Date() }`);
      });
  }

  get(id: string): Observable<Place> {
    return this.http.get(`${ API_URL }/places/${ id }`)
      .map((res: Response) => res.json())
      .catch((err: any) => Observable.throw(err.json()))
      .finally(() => {
        console.info(`Place.get: ${ new Date() }`);
      });
  }
}
