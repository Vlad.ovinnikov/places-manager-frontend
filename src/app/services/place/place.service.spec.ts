import { TestBed, inject, fakeAsync, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { PlaceService } from './place.service';
import { Place } from './../../models';

describe('PlaceService', () => {
  let mockBackend: MockBackend;
  let placeService: PlaceService;
  const fakeResponse = [
      {
        '_id': '592d29492be8ebb1dd2d1932',
        'icon': 'https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png',
        'name': '24/7 Sports Bar',
        'place_id': 'ChIJ77Cd7TauEmsRBV42CMtSans'
      }
    ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        PlaceService, MockBackend, BaseRequestOptions,
        {
          provide: Http,
          deps: [MockBackend, BaseRequestOptions],
          useFactory: (backend, options) => new Http(backend, options)
        }
      ]
    });
    mockBackend = TestBed.get(MockBackend);
    placeService = TestBed.get(PlaceService);
  }));

  afterEach(() => {
    mockBackend = null;
    placeService = null;
  });

  it('should be defined', () => {
    expect(placeService).toBeDefined();
  });

  it('should return list of items',  (done) => {
    mockBackend.connections.subscribe(connection => {
      connection.mockRespond(new Response(
        new ResponseOptions({
          body: fakeResponse
        })
      ));
    });

    placeService.getPlacesAPI('cafe', 1, 5).subscribe(
      (data: Place[]) => {
        expect(data.length).toBe(1);
        expect(data[0].name).toBe(fakeResponse[0].name);
        done();
      });
  });

  it('should return item',  (done) => {
    mockBackend.connections.subscribe(connection => {
      connection.mockRespond(new Response(
        new ResponseOptions({
          body: fakeResponse[0]
        })
      ));
    });

    placeService.get('592d29492be8ebb1dd2d1932').subscribe(
      (data: Place) => {
        expect(data).toBe(fakeResponse[0]);
        expect(data.name).toBe(fakeResponse[0].name);
        done();
      });
  });
});
