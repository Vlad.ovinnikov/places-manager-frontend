import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
// root app
import { AppComponent } from './app.component';
// views
import { ListComponent, DetailsComponent } from './views';
// routes
import { RoutesModule } from './app.routes';
// partial views
import { HeaderComponent } from './views/partials';
// services
import { PlaceService } from './services';
// components
import { SpinnerComponent } from './components';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailsComponent,
    HeaderComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    RoutesModule,
    InfiniteScrollModule,
    BrowserAnimationsModule
  ],
  providers: [
    PlaceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
