import { TestBed, async } from '@angular/core/testing';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';

// root app
import { AppComponent } from './app.component';
// partial views
import { HeaderComponent } from './views/partials';
// views
import { ListComponent, DetailsComponent } from './views';
// routes
import { RoutesModule } from './app.routes';
// services
import { PlaceService } from './services';
// components
import { SpinnerComponent } from './components';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RoutesModule,
        ReactiveFormsModule,
        InfiniteScrollModule,
        HttpModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        ListComponent,
        DetailsComponent,
        SpinnerComponent
      ],
      providers: [
        PlaceService, { provide: APP_BASE_HREF, useValue: '/places' }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
