export class Photo {

  _id: string;
  height: number;
  width: number;
  photo_reference: string;
  html_attributions: string;
}
