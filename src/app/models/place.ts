import { OpeningHours, Photo, Geometry } from './';

export class Place {

  _id: string;
  name: string;
  icon: string;
  place_id: string;
  reference: string;
  vicinity: string;
  createdAt: Date;
  updatedAt: Date;
  types: string[];
  opening_hours: OpeningHours;
  photos: Photo[];
  geometry: Geometry;
}
