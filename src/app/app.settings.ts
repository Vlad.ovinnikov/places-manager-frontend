import { environment } from './../environments/environment';

export const API_URL = environment.API_URL;
export const LOCATION = '-33.8671,151.1957';
export const RADIUS = '500';
export const TYPES = 'food';
export const GOOGLE_API = 'AIzaSyA18L3WnU7ibVBV56bn4cDDmDpsPYSaqs8';
