import { Routes, RouterModule } from '@angular/router';

import { ListComponent, DetailsComponent } from './views';

const APP_ROUTES: Routes = [

  { path: 'places', component: ListComponent },
  { path: 'places/:id', component: DetailsComponent },
  { path: '**', redirectTo: 'places' }

];

export const RoutesModule = RouterModule.forRoot(APP_ROUTES);
