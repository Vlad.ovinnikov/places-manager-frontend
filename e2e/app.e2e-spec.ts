import { PlacesManagerFrontendPage } from './app.po';

describe('places-manager-frontend App', () => {
  let page: PlacesManagerFrontendPage;

  beforeEach(() => {
    page = new PlacesManagerFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
